package tests;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.Homepage;
import pages.ProductDetailsPage;
import pages.SearchResultPage;
import utilities.Config;
import utilities.Driver;

public class SeeProductOnQuicklookOverlay {

	public static WebDriver driver = Driver.getDriver();

	Homepage home = new Homepage(driver);
	ProductDetailsPage product = new ProductDetailsPage(driver);
	SearchResultPage result = new SearchResultPage(driver);
	WebDriverWait wait = new WebDriverWait(driver, 30);

	@BeforeClass
	public void openTheMainPage() {
		driver.get(Config.getValue("url"));
		wait.until(ExpectedConditions.visibilityOf(home.minimize));
		home.clickMinimize();
	}

	@Test(priority = 1)
	public void SearchTakesToResultPage() {
		home.searchField.sendKeys(Config.getValue("searchItem")+Keys.ENTER);
		//home.searchButton.click();
		wait.until(ExpectedConditions.visibilityOf(result.searchResultHeading));
		Assert.assertTrue(result.verifySearchResultDisplayed(), "Search feild is not taking to the results page");
	}
	
	@Test(priority = 2)
	public void QuicklookLinkDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(result.quicklookLink));
		Assert.assertTrue(result.verifyQuicklooklinkDisplayed(), "Quicklook link is not taking to the results page");
	}
	
	@Test(priority = 3)
	public void ProductOverlayDisplayed() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scroll(0,300);");
		result.quicklookLink.click();
		wait.until(ExpectedConditions.visibilityOf(result.productOverlay));
		Assert.assertTrue(result.verifyProductOverlayDisplayed(), "Product overlay is not displayed");
	}
	
	@Test(priority = 4)
	public void ProductSameNamePriceInTheOverlay() {
		Assert.assertEquals(product.getProdName(), result.getFirstProductName(), "Name does not match");
		Assert.assertEquals(product.getProdPrice(), result.getFirstProductPrice(), "Price does not match");
	}
	

	@AfterClass
	public void close() {
		if (driver != null) {
			driver.quit();
		}
	}
}
