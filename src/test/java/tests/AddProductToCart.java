package tests;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.Homepage;
import pages.ProductDetailsPage;
import utilities.Config;
import utilities.Driver;

public class AddProductToCart {

	public static WebDriver driver = Driver.getDriver();

	Homepage home = new Homepage(driver);
	ProductDetailsPage product = new ProductDetailsPage(driver);
	WebDriverWait wait = new WebDriverWait(driver, 30);
	Actions action=new Actions(driver);
	String addedProduct;
	
	@BeforeClass
	public void openTheMainPage() {
		driver.get(Config.getValue("url"));
		wait.until(ExpectedConditions.visibilityOf(home.minimize));
		home.clickMinimize();
	}

	@Test(priority = 1)
	public void AddToCartButtonDisplayed() {	
		action.moveToElement(home.cooksTools).build().perform();
		home.SaltPepperMills.click();
		wait.until(ExpectedConditions.visibilityOf(home.product));
		home.product.click();
		addedProduct=product.productOverlayName.getText();
		wait.until(ExpectedConditions.visibilityOf(product.addToCartButton));
		Assert.assertTrue(product.verifyAddToCartButtonDisplayed(), "Add to card button failed to display");
	}

	@Test(priority = 2)
	public void AddToCartOverlayDisplayed() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scroll(0,700);");
		wait.until(ExpectedConditions.elementToBeClickable(product.addToCartButton));
		product.addToCartButton.click();
		wait.until(ExpectedConditions.visibilityOf(product.overlay));
		Assert.assertTrue(product.verifyAddToCartOverlayDisplayed(), "ADD TO CART OVERLAY failed to display");
	}

	@Test(priority = 3)
	public void CheckOutCartDisplayedOnTheOverlay() {
		wait.until(ExpectedConditions.visibilityOf(product.checkoutButton));
		Assert.assertTrue(product.verifyCheckoutButtonDisplayed(), "CHECKOUT button is not displayed on the overlay");
	}

	@Test(priority = 4)
	public void ShoppingCartPageDisplayed() {
		product.checkoutButton.click();
		wait.until(ExpectedConditions.visibilityOf(home.shoppingCart));
		Assert.assertTrue(home.verifyShoppingCartPageDisplayed(), "Shopping Cart is not displayed");
	}

	@Test(priority = 5)
	public void ProductDisplayedOnTheCartPage() {
		Assert.assertEquals(home.getAddedProductName() ,addedProduct);
	}

	@AfterClass
	public void close() {
		if (driver != null) {
			driver.quit();
		}
	}
}
