package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.Driver;

public class ProductDetailsPage {

	public ProductDetailsPage(WebDriver driver) {

		driver = Driver.getDriver();
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath = "//div[@class='pip-summary']/h1")
	public WebElement productOverlayName;

	@FindBy(id = "primaryGroup_addToCart_0")
	public WebElement addToCartButton;

	@FindBy(id = "racOverlay")
	public WebElement overlay;

	@FindBy(id = "btn-checkout")
	public WebElement checkoutButton;
	
	@FindBy(xpath = "//*[@class='currency currencyUSD ']")
	public WebElement productOverlayPrice;
	
	public String getProdPrice() {
		String price=productOverlayPrice.getText();
		return price;
	}
	
	public String getProdName() {
		String name=productOverlayName.getText();
		return name;
	}


	public boolean verifyAddToCartButtonDisplayed() {
		try {
			addToCartButton.isDisplayed();
			return true;
		} catch (NoSuchElementException e) {
			System.out.println("Element could not be located");
			return false;
		}
	}

	public boolean verifyAddToCartOverlayDisplayed() {
		try {
			overlay.isDisplayed();
			return true;
		} catch (NoSuchElementException e) {
			System.out.println("Element could not be located");
			return false;
		}
	}

	public boolean verifyCheckoutButtonDisplayed() {
		try {
			checkoutButton.isDisplayed();
			return true;
		} catch (NoSuchElementException e) {
			System.out.println("Element could not be located");
			return false;
		}
	}

}
