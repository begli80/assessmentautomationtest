package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.Driver;

public class SearchResultPage {
	
		public SearchResultPage (WebDriver driver) {

			driver = Driver.getDriver();
			PageFactory.initElements(driver, this);
		}

		@FindBy(id = "results-summary")
		public WebElement searchResultHeading;
		
		@FindBy(xpath= "//*[@class='quicklook-link']")
		public WebElement quicklookLink;
		
		@FindBy(xpath = "//*[@class='shop-list product-list']/li[1]//a[@class='product-name']")
		public WebElement firstProductName;

		@FindBy(xpath = "//*[@class='shop-list product-list']/li[1]//div[@class='product-copy']/span/span[@class='price-state price-standard']")
		public WebElement firstProductPrice;
			
		@FindBy(id = "quicklookOverlay")
		public WebElement productOverlay;
		
		
		
		public boolean verifyProductOverlayDisplayed() {
			try {
				productOverlay.isDisplayed();
				return true;
			} catch (NoSuchElementException e) {
				System.out.println("Element could not be located");
				return false;
			}
		}
		
		public boolean verifySearchResultDisplayed() {
			try {
				searchResultHeading.isDisplayed();
				return true;
			} catch (NoSuchElementException e) {
				System.out.println("Element could not be located");
				return false;
			}
		}
		
		public boolean verifyQuicklooklinkDisplayed() {
			try {
				quicklookLink.isDisplayed();
				return true;
			} catch (NoSuchElementException e) {
				System.out.println("Element could not be located");
				return false;
			}
		}
		
		public String getFirstProductName() {
			String firstProdName = firstProductName.getText();
			return firstProdName;
		}
		
		public String getFirstProductPrice() {
			String firstProdPrice = firstProductPrice.getText();
			return firstProdPrice;
		}
	}

