package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.Driver;

public class Homepage {

	public Homepage(WebDriver driver) {

		driver = Driver.getDriver();
		PageFactory.initElements(driver, this);
	}

	@FindBy(linkText = "Salt & Pepper Mills")
	public WebElement SaltPepperMills;

	@FindBy(xpath = "//a[@class='stickyOverlayMinimizeButton']")
	public WebElement minimize;

	@FindBy(xpath = "//*[@class='topnav-cooks-tools ']")
	public WebElement cooksTools;

	@FindBy(xpath = "//ul[@class='shop-list product-list ']/li[2]")
	public WebElement product;

	@FindBy(xpath = "//*[@class='checkout-page-header']")
	public WebElement shoppingCart;

	@FindBy(xpath = "//div[@class='cart-table-row-title']")
	public WebElement shoppingCartProduct;

	@FindBy(id = "search-field")
	public WebElement searchField;

	@FindBy(id = "btnSearch")
	public WebElement searchButton;

	

	public void clickMinimize() {
		minimize.click();
	}

	public boolean verifyShoppingCartPageDisplayed() {
		try {
			shoppingCart.isDisplayed();
			return true;
		} catch (NoSuchElementException e) {
			System.out.println("Element could not be located");
			return false;
		}
	}

	public String getAddedProductName() {
		String prodName = shoppingCartProduct.getText();
		return prodName;
	}

}
