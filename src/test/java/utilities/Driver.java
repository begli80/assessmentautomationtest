package utilities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Driver {
	
		static WebDriver driver;
		
		public static WebDriver getDriver () {
			
			if(driver!=null) {
				return driver;
			}
			System.setProperty("webdriver.chrome.driver", "C:\\learnjava\\AssessmentAutomationTest\\drivers\\chromedriver.exe");
			 driver=new ChromeDriver();
			 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			 driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
			 driver.manage().window().fullscreen();
			 return driver;
		}
		

}
